250g sweet peppers
100g tomato

jalapeno
habanero
Serrano

~30g onion
~10g garlic
~10g cilantro
~12g lime
~8g salt

Roast peppers, remove skin and seeds
Roast tomatoes, remove skin and core
Puree peppers, tomato
Mince onion, garlic, cilantro, add to puree
Add seasoning, adjust to taste
