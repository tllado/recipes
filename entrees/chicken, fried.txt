Fried Chicken

Brine
    4c water
    50g salt
Soak for 90 minutes

Marinade
    2tbsp cayenne
    2tbsp hungarian hot paprika
    0.75tbsp cumin
    0.75tsp sage
    0.5tsp rosemary
    0.5tsp thyme
    0.5tsp garlic powder
    2tsp salt
    2c buttermilk
Cut chicken into 12 pieces (breast in two + separate fillet)
Grind all spices to fine powder
Marinate 12-18hrs

Frying
    0.33gal peanut oil
    200g white flour
    70g semolina
    70g cornstarch
Let chicken in marinade come to room temperature
Preheat oil at beginning of each batch
Remove chicken from marinade, toss in flour
Fry in batches
    wings and strips, 330ºF ~6min
    breast pieces, 350ºF ~10min
    leg pieces, 350ºF ~13min
Dry on paper towels ~10min before serving
