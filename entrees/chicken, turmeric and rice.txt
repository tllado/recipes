Turmeric Chicken and Rice

One 4 1/2-pound chicken, cut into 8 pieces
Kosher salt
Freshly ground pepper
2 tablespoons unsalted butter
1 1/2 teaspoons ground turmeric
1 small onion, chopped
1 tablespoon finely chopped peeled fresh ginger
4 garlic cloves, minced
2 plum tomatoes, chopped
2 teaspoons curry powder
1/2 teaspoon ground cinnamon
1/2 teaspoon ground cumin
2 cups jasmine rice
3 bay leaves
1 1/2 tablespoons Asian fish sauce
3 cups chicken stock
Plain whole yogurt, sliced cucumbers, mint leaves and lime wedges, for serving

Season the chicken with salt and pepper. In a large enameled cast-iron casserole or Dutch oven, melt the butter and sprinkle with the turmeric. Add the chicken skin side down and cook over moderately high heat, turning once, until browned on both sides, 8 minutes total. Transfer the chicken to a plate.
Add the onion, ginger and garlic to the casserole and cook, stirring occasionally, until starting to brown, about 5 minutes. Add the tomatoes, curry powder, cinnamon, cumin and rice and stir constantly until fragrant, about 1 minute. Return the chicken to the pot, skin side up. Add the bay leaves, fish sauce and chicken stock and bring to a boil over high heat.
Cover and simmer over low heat for 10 minutes. Adjust the lid to cover partially and simmer until the rice is cooked, 10 to 15 minutes longer. Remove from the heat, uncover and let stand for 5 minutes. Serve with yogurt, cucumbers, mint and lime wedges.



2 tablespoons olive oil
1 whole cut-up chicken
½ teaspoon fine grain salt
¼ teaspoon ground black pepper
2 medium onions, finely chopped
3 garlic cloves, minced
1 ½ cups short-grain rice
1 teaspoon ground turmeric
Pinch of saffron threads (optional)
3 ½ cups chicken or vegetable stock
1 cup frozen peas (no need to thaw them)
Limes quartered and chopped fresh parsley, for serving

In a small bowl soak saffron threads and in a little bit of hot water (this really opens up the spice and releases its flavor). Set aside.
Heat olive oil in a large skillet over medium-high heat.
Add chicken skin side down, sprinkle with salt and black pepper, and cook undisturbed for about 8 to 10 minutes, or until the chicken pieces easily release from the pan (make sure that the chicken sizzles but doesn’t burn by adjusting the heat accordingly.)
Flip the chicken pieces and cook on the other side until browned, about 6 to 8 minutes.
Remove the chicken from the pan and set aside.
Spoon most of the fat from the skillet, leaving about 2 tablespoons for added flavor.
Reduce the heat to medium, add onions, and sauté for about 5 to 6 minutes, until translucent. 
Add garlic and sauté for 1 minute, until fragrant.
Add rice and cook stirring until the rice is glossy and coated with oil.
Add turmeric and saffron and give a good stir.
Return the chicken to the pan, add stock, and stir gently to combine.
Bring to a boil, reduce to a simmer, cover, and cook undisturbed for 20 minutes (resist the urge to uncover the skillet and stir, be patient!)
Uncover and check. The goal is to have the liquid absorbed, the rice is tender, and the chicken is cooked through.
If the rice is dry but nothing is ready, add another ¼ cup of water and cook for another 5 to 10 minutes.
When ready, take a taste and adjust seasoning as needed.
Remove from the heat, add peas, cover, and let sit for 10 to 15 minutes.
Fish the chicken out of the pan and fluff the rice with a fork.
Return chicken to the skillet, arrange lime wedges around, and sprinkle with chopped fresh parsley.



2 teaspoons olive oil
6 chicken thighs (bone in, skin on) Use boneless, skinless if desired
salt and pepper
1/2 teaspoon garlic powder
1 onion, diced
2 cloves garlic, minced
2 teaspoons turmeric powder
1 1/4 cup long grain white rice
4 cups chicken stock I use low fat, low sodium
fresh cilantro or parsley for garnish

Heat a large cast iron skillet over medium high heat and add the oil. Preheat your oven to 375 degrees Fahrenheit.
Season the chicken thighs with salt, pepper and garlic powder on both sides. 
Add the chicken to the skillet and brown on both sides until golden. Don't worry about cooking the chicken through at this stage - just focus on getting that crispy brown crust on the outside.
Remove the chicken to a plate once it's browned and set aside.
Add the onion and garlic to the skillet, and turn the heat to medium.
Saute the onions and garlic until the onions begin to soften. Add the turmeric and rice and stir everything together well, letting the rice toast just a little bit.
Add the chicken stock and stir well to combine. Turn off the heat under the skillet.
Nestle the chicken pieces into the liquid-y rice mixture in the pan, making sure the tops of the chicken pieces are sticking out of the liquid just a little bit.
Place the skillet in the oven for 20-25 minutes or until the chicken pieces are cooked through to an internal temperature of 165 Fahrenheit (75 Celsius) and the rice is cooked.
Serve with a sprinkling of freshly chopped cilantro or parsley for freshness, and serve with a side salad or some veggies.



For Chicken
1/4 cup olive oil
2 teaspoons dried parsley
1 1/2 teaspoons paprika
1 teaspoon garlic powder
1/2 teaspoon dried thyme
1 teaspoon salt
1/4 teaspoon pepper to taste
6 skinless chicken thighs, bone in or out
For Rice
1 large onion chopped
4 cloves garlic minced
2 tablespoons oil
1 teaspoon salt
1/4 teaspoon cracked black pepper
2 cups chicken broth (or stock)
1 1/2 cups hot water
2 cups long grain white rice
For Mushrooms (OPTIONAL)
2 tablespoons butter
2 cloves garlic, minced
14 ounces (400g) button mushrooms quartered
1/4 cup chives, divided
Salt and pepper, to taste
2 tablespoons fresh chopped parsley, to garnish

Preheat oven to 350°F | 180°C.
In a shallow bowl, mix together the olive oil, parsley, paprika, garlic powder, thyme, salt and pepper. Add the chicken and coat each piece evenly with the seasoning. Set aside, allowing the flavours to absorb into the meat.
Spray a large baking dish (10-inch x 15-inch or 25 x 35 cm) with nonstick cooking oil spray. Add the onion, garlic, oil, salt, pepper, broth (or stock) and water into the dish. Stir well to combine. Then mix in the rice.
Arrange thighs in the water over the rice, cover with foil and bake for 50 minutes. Uncover and bake for an additional 20-30 minutes, until the chicken is cooked right through to the bone. Change oven setting to broil for 2-3 minutes to brown the chicken and crisp rice.
Remove chicken thighs, mix the rice through, then arrange the chicken back on top of the rice. Cover and allow it to rest for 5-10 minutes to set all of the flavours in the hot dish. The rice will finish off cooking while resting.
While chicken is resting, prepare mushrooms (OPTIONAL):
Heat butter in a pan over medium-high heat. Sauté garlic until fragrant (30 seconds) and add mushrooms; cook until softened. Stir in 2 tablespoons of chives and season with salt and pepper.
Transfer chicken onto a plate; fluff rice with forks,, then mix the mushrooms through the rice. Arrange chicken back onto the rice in the dish, garnish with remaining chives and chopped parsley. SERVE and enjoy!