Mushroom Risotto

6c broth, half strength
3tbsp olive oil
1lb portobello mushrooms
1lb white mushrooms (if use shitake, remove stems)
2 shallots
1.5c Arborio rice
0.5c dry white wine
3tbsp chives
4tbsp butter
0.33c Parmesan

1 - In a saucepan, warm the broth over low heat.

2 - Warm 2 tablespoons olive oil in a large saucepan over medium-high heat. Stir in the mushrooms, and cook until soft, about 3 minutes. Remove mushrooms and their liquid, and set aside.

3 - Add 1 tablespoon olive oil to skillet, and stir in the shallots. Cook 1 minute. Add rice, stirring to coat with oil, about 2 minutes. When the rice has taken on a pale, golden color, pour in wine, stirring constantly until the wine is fully absorbed. Add 1/2 cup broth to the rice, and stir until the broth is absorbed. Continue adding broth 1/2 cup at a time, stirring continuously, until the liquid is absorbed and the rice is al dente, about 15 to 20 minutes.

4 - Remove from heat, and stir in mushrooms with their liquid, butter, chives, and parmesan. Season with salt and pepper to taste.