For a single skillet ...
1/4 chicken or two large pieces, chopped into chunks or in large pieces
handful of green beans
handful of butter beans, fresh or fully cooked
one large tomato, grated
large pinch of saffron
spanish paprika, ~1tbsp
2.5c water
salt to taste
1 large sprig of rosemary
1c arborio rice
lemon

Heat pan very hot with oil, just enough to cover the entire bottom with a very thin layer (not a thick layer, last time was too much)
Fry chicken until browned all over
Fry beans for a few minutes until very softened
Fry garlic for a minute
Add tomato and spices
Add water and rosemary
Simmer low for 20 minutes and season to taste
Add rice, stir once, and let simmer low for 20 minutes
Garnish with lemon
