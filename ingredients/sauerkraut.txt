Sauerkraut

Cabbage, sliced
Water, boiled and cooled
Salt

Toss cabbage with salt, let sit for 1hr
Press into jar
Top with weight
Cover with water
Close and seal with airlock
