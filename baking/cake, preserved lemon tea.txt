Preserved Lemon Tea Cake

0.5c extra-virgin olive oil, plus more for parchment
1 preserved lemon (about 55 g)
190g all purpose flour
2tsp baking powder
0.5tsp ground turmeric
3 large eggs
225g granulated sugar
0.5c sour cream
1tbsp finely grated lemon zest
3tbsp fresh lemon juice
83g powdered sugar
1tbsp whole milk
Flaky sea salt

Place a rack in middle of oven; preheat to 350°. Line an 8½x4½" loaf pan, preferably metal, with parchment paper, leaving generous overhang on the long sides, and brush with oil. Cut preserved lemon into quarters; remove any seeds. Transfer to a small food processor and process to form a paste (you can also do this with a mortar and pestle or simply chop and smash with your knife).

Whisk flour, baking powder, and turmeric in a medium bowl to combine. Beat eggs, granulated sugar, and remaining ½ cup oil in the bowl of a stand mixer fitted with the paddle attachment on medium speed until smooth and incorporated, about 1 minute. Add sour cream and mix to combine. Add preserved-lemon paste, lemon zest, and lemon juice and mix to combine. Reduce speed to low, add dry ingredients, and mix until just combined, about 15 seconds. (Batter can also be mixed together in a large bowl with a whisk.) Scrape batter into prepared pan and smooth top.

Bake cake until top is golden brown and a tester inserted into the center comes out clean, 50–60 minutes. Transfer pan to a wire rack and let cake cool 15 minutes. Run a knife around sides of pan to loosen and, using parchment paper overhang, lift cake out of pan and onto rack. Peel away parchment paper and discard. Let cake cool completely.

Meanwhile, whisk powdered sugar and milk in a medium bowl until smooth.

Transfer cake to a platter or large plate. Using a rubber spatula to help guide glaze, spoon glaze over cake, letting it drip down the sides (you should have a fairly thick coating). Sprinkle sea salt over glaze and let cake sit until glaze is set, about 30 minutes.

