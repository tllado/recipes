A bit runny around the edges, but that makes the edges very crispy ...
Very mushy in the center ... oddly colored
Need to figure out how to hold ingredients together better
Dry out very quickly, only last 1-2 days

360g powdered sugar
55g unsweetened cocoa powder
1tsp kosher salt
3 large egg whites, room temperature
2tsp vanilla extract
5g espresso powder
350g dark chocolate, finely chopped

Pre-heat oven to 350F
Mix dry ingredients
Stir in wet ingredients
Spoon very small scoops onto buttered baking sheet
Bake 11-13min until tops are crackly and consistent light brown
Let cool 10min before removing from baking sheet
