2 eggs
0.75c butter
1tsp vanilla
150g white sugar
150g brown sugar

275g oats
125g raisins
225g flour
0.5tsp baking soda
0.5tsp baking powder
1tsp cinnamon
0.75tsp salt

Cream eggs, butter, sugar
Mix and add dry ingredients
Refrigerate for 1hr
Bake 350ºF ~14minutes
