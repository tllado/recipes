Mexican Chocolate Icebox Cake
Courtesy of: Central Market
8 servings

3 (3-ounce) packages ladyfingers (about 60)
1/3 cup Kahl�a, optional
2 tablespoons cinnamon sugar
2 2/3 cups chilled heavy whipping cream
4 ounces unsweetened chocolate, chopped
1/4 cup sugar
1 1/4 cup confectioners' sugar
1/2 cup (1 stick) unsalted butter
2 teaspoons vanilla extract
1 teaspoon cinnamon
1 tablespoon grated semisweet chocolate

Drizzle the top side of the ladyfingers with some of the Kahl�a. Sprinkle with cinnamon sugar. Line the bottom and side of a 9-inch springform pan with the ladyfingers, reserving 15. Heat 3/4 cup of the whipping cream and unsweetened chocolate and sugar in a saucepan over low heat until chocolate melts and mixture is smooth, stirring constantly. Let stand until room temperature.

Beat 1 cup plus 2 tablespoons of the confectioners' sugar, butter and 1 teaspoon of the vanilla in a bowl until well mixed. Beat in the cooled chocolate mixture. Combine the remaining whipping cream, remaining 2 tablespoons confectioners' sugar, remaining 1 teaspoon vanilla and cinnamon in a separate bowl. Beat until firm peaks form. Fold 3/4 of the whipped cream mixture into the chocolate mixture. Spread half the chocolate mixture in the prepared pan. Top with the reserved ladyfingers. Spread the remaining chocolate mixture over the ladyfingers. Decorate the top with the remaining whipped cream mixture. Sprinkle with grated chocolate. Chill, covered, for 3 to 24 hours or until firm.
