70g white sugar
55g brown sugar
90g honey
10g water
1tsp ground cinnamon
3/4tsp ground ginger
1/2tsp ground cardamom
1/4tsp ground nutmeg
1/4tsp ground allspice
1/4tsp ground clove
1/8tsp white pepper
5g (1tsp) baking soda
6g (1.5tsp) salt
2tbsp heavy cream
1 large egg, beaten
2.5c all purpose flour

300g powdered sugar
3tbsp lemon juice

Combine sugars, water, spices in saucepan, heat until begins to simmer, then let cool down to warm
Add salt, baking soda, heavy cream, and egg and whisk until thoroughly combined
Add flour and mix together minimally
Wrap in plastic and refrigerate 1-2 days

Peheat oven to 375ºF
Mix icing
Line 2 baking sheets with parchment paper
Roll dough into ~1inch balls, arrange on sheet, will ~double in size
Bake until cookies barely start to brown, ~10min
Let cool all the way
Dunk in icing and let dry for 1-2hrs

