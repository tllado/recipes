Cornbread

160g cornmeal
120g flour
15g sugar
5g baking powder
3g baking soda
2g salt
80g butter
2 eggs
350g buttermilk

Preheat oven to 425F
Grease pan with butter
Mix dry ingredients
Mix wet ingredients, fold into dry
Bake 20-25 minutes, cool 10 minutes
